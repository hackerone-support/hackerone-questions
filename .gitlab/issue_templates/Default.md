<!---

IMPORTANT! To report a vulnerability, please use HackerOne: https://hackerone.com/gitlab

Your question might already be answered here:

- https://hackerone.com/gitlab
- https://about.gitlab.com/security/disclosure/
- https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/runbooks/hackerone-process.html
- https://gitlab-com.gitlab.io/gl-security/appsec/cvss-calculator/

-->

<!-- Your question goes here :) -->




<!-- The following line will alert AppSec to your question -->
cc @gitlab-com/gl-security/appsec

<!-- 
The following line will make this issue confidential.
You can remove it if your question is not sensitive.
-->
/confidential
