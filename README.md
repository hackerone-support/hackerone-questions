# HackerOne Questions

Welcome! This project lets security researchers ask GitLab's AppSec team questions about our HackerOne programme. 

Ask a question by [creating a new issue](https://gitlab.com/gitlab-com/gl-security/appsec/hackerone-questions/-/issues/new).

## :warning: NO VULNERABILITIES HERE

Follow our [Responsible Disclosure Policy](https://about.gitlab.com/security/disclosure/) for disclosing vulnerabilities or issues that you believe is important to communicate to the Security team. Do not report vulnerabilities in this project's issue tracker.

To discuss a vulnerability you've already reported, make a comment in the existing HackerOne Report. Any attempt to circumvent our HackerOne process by creating discussion here will result in your issue being closed without comment. It is also a **violation** of our HackerOne policy:

> The only appropriate place to inquire about a HackerOne report's status is on the report itself. Please refrain from submitting your report or inquiring about its status through additional channels including any other unrelated HackerOne report, as this unnecessarily binds resources in the security team.

## Is your question already answered?

Your question might already be answered in the following links:

- https://hackerone.com/gitlab
- https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/runbooks/hackerone-process.html
- https://gitlab-com.gitlab.io/gl-security/appsec/cvss-calculator/

## Other GitLab questions

If you have a question about something other than vulnerability disclosure, please refer to one of the following:

- [GitLab Support](https://about.gitlab.com/support/)
- [GitLab Customer Assurance Package](https://about.gitlab.com/security/cap/)
- [GitLab Press Enquiries](https://about.gitlab.com/press/#get-in-touch)
